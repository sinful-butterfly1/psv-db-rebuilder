#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vitasdk.h>

#include "ctrl.h"
#include "debugScreen.h"

#define printf psvDebugScreenPrintf
#define clearScreen psvDebugScreenClear

int main(){
  psvDebugScreenInit();
  clearScreen(0);
  printf("The Database will be rebuilt in 3 seconds.");
  sceKernelDelayThread(3000000); // 3 seconds
  sceIoRemove("ux0:/id.dat");
  sceIoRemove("ur0:/shell/db/app.db");
  scePowerRequestColdReset();
  return 0;
}


